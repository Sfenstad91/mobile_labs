package com.example.stian.lab2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Stian on 18.03.2018.
 */

public class WebviewActivity extends AppCompatActivity{
    String urlS;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        Intent intent = getIntent();
        urlS = intent.getStringExtra("URL");

        // How to set up webView taken and modified from
        // https://developer.android.com/guide/webapps/webview.html
        // This includes the usage of javascriptenable.
        // Will ignore the warning of XSS vulnerability at this time.
        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        webView.loadUrl(urlS);

    }
}
