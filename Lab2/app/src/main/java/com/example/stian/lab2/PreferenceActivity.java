package com.example.stian.lab2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by Stian on 17.03.2018.
 */

public class PreferenceActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF = "state";
    SharedPreferences state;
    EditText editUrl;
    Button back, save;
    Spinner rss, disp, freq;
    String[] arrayRss, arrayFreq;
    Integer[] arrayDisp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        state = getApplicationContext().getSharedPreferences(PREF, MODE_PRIVATE);
        initialize_page_elements();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                back();
                break;
            case R.id.save:
                save_pref();
                break;
            default:
                break;
        }
    }
    public void back() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    /*
     *Saves spinner settings and returns values to main activity
     */
    public void save_pref(){
        Intent intent= new Intent();
        SharedPreferences.Editor editor = getSharedPreferences(PREF, MODE_PRIVATE).edit();
        String text = editUrl.getText().toString();
        int s1 = rss.getSelectedItemPosition();
        int s2 = disp.getSelectedItemPosition();
        int s3 = freq.getSelectedItemPosition();

        editor.putString("url_v", text);
        editor.putInt("rss_v", s1);
        editor.putInt("disp_v", s2);
        editor.putInt("freq_v", s3);
        editor.apply();

        intent.putExtra("urlString", text);
        intent.putExtra("rssValue", rss.getSelectedItem().toString());
        intent.putExtra("freqValue", freq.getSelectedItem().toString());
        intent.putExtra("dispValue", (Integer)disp.getSelectedItem());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
    /*
    *initializes all page elements and their values
     */
    public void initialize_page_elements() {
        back = (Button) findViewById(R.id.back);
        back.setOnClickListener(this);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
        editUrl = (EditText)findViewById(R.id.url);
        rss = (Spinner) findViewById(R.id.rssS1);
        disp = (Spinner) findViewById(R.id.dispS2);
        freq = (Spinner) findViewById(R.id.frequencyS3);

        arrayRss = new String[] {
               "RSS2.0", "Atom"
        };
        arrayFreq = new String[] {
                "10min", "60min", "24h"
        };
        arrayDisp = new Integer[]{
              10, 20, 50, 100
        };

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayRss);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayFreq);
        ArrayAdapter<Integer> adapter3= new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, arrayDisp);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        rss.setAdapter(adapter1);
        freq.setAdapter(adapter2);
        disp.setAdapter(adapter3);


        if(state.contains("rss_v") && state.contains("disp_v") && state.contains("freq_v") && state.contains("url_v")){
            editUrl.setText(state.getString("url_v", "DEFAULT"));
            rss.setSelection(state.getInt("rss_v",0));
            disp.setSelection(state.getInt("disp_v",0));
            freq.setSelection(state.getInt("freq_v",0));
        }
    }
}
