package com.example.stian.lab2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    static final int ACTIVITY_RESULT = 1;
    Button fetch, pref, exit;
    private Handler fetchHandler = new Handler();
    ListView rssList;
    URL url;
    String urlS;
    ArrayList<String> titles;
    ArrayList<String> links;
    int displayCount;
    int displayFreq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fetch = (Button) findViewById(R.id.fetch);
        fetch.setOnClickListener(this);
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(this);
        pref = (Button) findViewById(R.id.pref);
        pref.setOnClickListener(this);


    }
    /**
     * Updates listview according to preferences
     * @param req
     * @param result
     * @param data
     */
    @Override
    protected void onActivityResult(int req, int result, Intent data) {
        if(req == ACTIVITY_RESULT){
            if(result == RESULT_OK) {
                Bundle extras = data.getExtras();
                urlS = extras.getString("urlString");
                displayCount = extras.getInt("dispValue");
                displayFreq = parse_frequency(extras.getString("freqValue"));
                titles = new ArrayList<>();
                links  = new ArrayList<>();
                rssList = (ListView) findViewById(R.id.rssListView);
                //Listener for items in rss feed
                rssList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String link = links.get(i);
                        Intent intent = new Intent(getApplicationContext(), WebviewActivity.class);
                        intent.putExtra("URL", link);
                        startActivity(intent);
                    }
                });
                Feed.run();
            }
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fetch:
                fetch_rss();
                break;
            case R.id.pref:
                display_preferences();
                break;
            case R.id.exit:
                exit_app();
                break;
            default:
                break;
        }
    }
    /*
    * Refreshes rss feed(requires url in preferences)
     */
    public void fetch_rss(){
        Feed.run();
    }
    /*
    * returns frequency value in milliseconds
    */
    public Integer parse_frequency(String frequency){
        int tmp;
        if(frequency.equalsIgnoreCase("10min"))
            return  tmp = 600000;
        else if(frequency.equalsIgnoreCase("60min"))
            return tmp = 3600000;
        else if(frequency.equalsIgnoreCase("24h"))
            return tmp = 86400000;
        else
            return tmp = 600000;
    }

    public void display_preferences(){
        Intent intent = new Intent(MainActivity.this, PreferenceActivity.class);
        startActivityForResult(intent, ACTIVITY_RESULT);
    }

    public void exit_app(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Method for fetching feed, this is taken and modified from tutorial on RSS feed in Java.
     * https://www.androidauthority.com/simple-rss-reader-full-tutorial-733245/
     */
    public class FetchFeedTask extends AsyncTask<Integer, Integer, Exception> {
        Exception exc = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Exception s) {
            super.onPostExecute(s);
            ArrayAdapter<String> adapter= new ArrayAdapter<>(
                    MainActivity.this, android.R.layout.simple_list_item_1, titles);
            rssList.setAdapter(adapter);
        }

        @Override
        protected Exception doInBackground(Integer... integers) {
            try {
                if(urlS==null)
                    url = new URL("https://www.vg.no/rss/feed/");
                else
                    url = new URL(urlS);
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(false);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(getInputStream(url), "UTF_8");
                boolean insideItem = false;
                int eventType = xpp.getEventType();
                while(eventType != XmlPullParser.END_DOCUMENT && links.size()<displayCount) {
                    if(eventType == XmlPullParser.START_TAG) {
                        if(xpp.getName().equalsIgnoreCase("item")) {
                            insideItem = true;
                        }
                        else if(xpp.getName().equalsIgnoreCase("title")) {
                            if(insideItem) {
                                titles.add(xpp.nextText());
                            }
                        }
                        else if ( xpp.getName().equalsIgnoreCase("link")) {
                            if(insideItem) {
                                links.add(xpp.nextText());
                            }
                        }
                    }
                    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase(
                            "item")) {
                        insideItem= false;
                    }
                    eventType = xpp.next();
                }
            }catch (MalformedURLException e) {
                exc = e;
            }catch (XmlPullParserException e) {
                exc = e;
            }catch(IOException e) {
                exc = e;
            }
            return exc;
        }
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            return null;
        }
    }
    private Runnable Feed = new Runnable() {
        @Override
        public void run() {
            new FetchFeedTask().execute();
            fetchHandler.postDelayed(this, displayFreq);
        }
    };
}

