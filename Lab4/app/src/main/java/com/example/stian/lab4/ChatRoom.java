package com.example.stian.lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Created by Stian on 03.04.2018.
 */

public class ChatRoom extends AppCompatActivity {
    private String username;
    private FirebaseAuth mAuth;
    private FirebaseListAdapter<ChatMessage> adapter;
    FloatingActionButton b1;
    SharedPreferences state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatroom);
        state = PreferenceManager.getDefaultSharedPreferences(this);
        username = state.getString("USERNAME", "");
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously();



        b1 = findViewById(R.id.b1);

        displayChatMsg();

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = findViewById(R.id.t1);
                //Read the input field and push new instance of chatmsg to firebase db
               FirebaseDatabase.getInstance()
                        .getReference("messages")
                        .push()
                        .setValue(new ChatMessage(input.getText().toString(), username)
                        );
                //Clear input
                input.setText("");
            }
        });
    }


    /**
     * This method was added due to issues with the adapter to fill the view.
     * This is needed but not documented with google.
     */

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startListening();
        }
    }
    /**
     * Method to stop the listening of the adapter.
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }

    private void displayChatMsg(){
        ListView listOfMessages = findViewById(R.id.l1);

        Query query = FirebaseDatabase.getInstance().getReference("messages");
        FirebaseListOptions<ChatMessage> options = new FirebaseListOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .setLayout(R.layout.messages)
                .build();

        adapter = new FirebaseListAdapter<ChatMessage>(options) {

            @Override
            protected void populateView(View v, ChatMessage model, int position) {
                // Get references to the views of message.xml
                TextView messageText = v.findViewById(R.id.text);
                TextView messageUser = v.findViewById(R.id.user);
                TextView messageTime = v.findViewById(R.id.time);
                // Set their text
                messageText.setText(model.getMsgText());
                messageUser.setText(model.getMsgUser());
                // Format the date before showing it
                android.text.format.DateFormat df = new android.text.format.DateFormat();

                messageTime.setText(df.format("dd-MM-yyyy (HH:mm:ss)", model.getMsgTime()));
            }
        };

        listOfMessages.setAdapter(adapter);
    }

    /**
     * Method to create a menu to sign out.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Method to select the menu item and sign out.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        if (item.getItemId() == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(ChatRoom.this,
                                    "You have been signed out.",
                                    Toast.LENGTH_LONG)
                                    .show();
                            // Close activity
                            finish();
                        }
                    });
            //Clear all sharepreferences
            state.edit().clear().commit();
        }
        return true;
    }

    public void listFriends(View view){
        Intent intent = new Intent(ChatRoom.this, ListFriends.class);
        startActivity(intent);
    }
}
