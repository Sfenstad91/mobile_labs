
package com.example.stian.lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity{
    static final int USER_RESULT = 1;
    private String username;
    // Firebase data.
    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private DatabaseReference users = db.getReference("users");
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously();

        getState();
        if(username.equals("")){
            getUsername();
        }else{
            displayChat();
        }

    }

    /**
     * Result of username query
     * @param req
     * @param result
     * @param data
     */
    @Override
    protected void onActivityResult(int req, int result, Intent data){
        if(req == USER_RESULT){
            if(result == RESULT_OK){
                saveState(data.getStringExtra("username"));
            }
        }
    }

    /**
     * Starts new activity and queries user for username
     */
    private void getUsername() {
        Intent intent = new Intent (MainActivity.this, Login.class);
        startActivityForResult(intent, USER_RESULT);
    }
    /**
     *After user validation - enter chatroom
     */
    private void displayChat() {
        Intent intent = new Intent(this, ChatRoom.class);
        startActivity(intent);
    }
    /**
     * Save username in sharedpreferences
     */
    private void saveState(String str){
        SharedPreferences state = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = state.edit();
        editor.putString("USERNAME", str);
        editor.apply();

        users.push().setValue(new User(str));
        displayChat();
    }

    /**
     * See if user exists in sharepreferences
     */
    private void getState() {
        SharedPreferences state = PreferenceManager.getDefaultSharedPreferences(this);
        username = state.getString("USERNAME", "");
    }

}
