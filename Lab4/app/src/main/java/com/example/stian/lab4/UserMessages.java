package com.example.stian.lab4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashSet;

/**
 * Created by Stian on 05.04.2018.
 */

public class UserMessages extends AppCompatActivity{
    private FirebaseListAdapter<ChatMessage> adapter;
    String newString;
    Bundle extras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usermessages);

        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                newString = null;
            } else {
                newString = extras.getString("Username");
            }
        } else {
            newString = (String) savedInstanceState.getSerializable("Username");
        }
        Log.d("USERNAME", newString);
        displayUserMessages();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    private void displayUserMessages() {
        Log.d("USERNAME", newString);

        ListView listOfMessages = findViewById(R.id.userMessages);
        Query query = FirebaseDatabase.getInstance().getReference("messages").orderByChild("msgUser").equalTo(newString);

        FirebaseListOptions<ChatMessage> options = new FirebaseListOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .setLayout(R.layout.messages)
                .build();
        adapter = new FirebaseListAdapter<ChatMessage>(options) {

            @Override
            protected void populateView(View v, ChatMessage model, int position) {

                // Get references to the views of message.xml
                TextView messageText = v.findViewById(R.id.text);
                TextView messageTime = v.findViewById(R.id.time);

                // Set their text
                messageText.setText(model.getMsgText());
                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMsgTime()));
            }
        };

        listOfMessages.setAdapter(adapter);
    }


}
