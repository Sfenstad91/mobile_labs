package com.example.stian.lab4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.Random;

/**
 * Created by Stian on 05.04.2018.
 */

/**
 * Sets username
 */
public class Login extends AppCompatActivity implements View.OnClickListener {
    private String username;
    private String[] names;
    Button save;
    EditText user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        user = findViewById(R.id.t2);
        save = findViewById(R.id.save);

        names = getResources().getStringArray(R.array.names);
        int rand = new Random().nextInt(names.length);
        String randomName = names[rand];
        user.setText(randomName);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save_username();
            }
        });
    }
    /**
     * Save specified username
     */
    private void save_username() {
        username = user.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("username", username);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    /**
     * Unused
     * @param view
     */
    @Override
    public void onClick(View view) {
    }
}
