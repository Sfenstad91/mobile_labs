package com.example.stian.lab4;


import android.support.v7.app.AppCompatActivity;
import android.view.View;


/**
 * Created by Stian on 03.04.2018.
 */

/**
 * User collection
 */
public class User {
    public String username;

    public String getUser(){
        return username;
    }

    public User(String username) {
        this.username = username;
    }
    public User(){

    }
}

