package com.example.stian.lab4;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Created by Stian on 05.04.2018.
 */

public class ListFriends extends AppCompatActivity{
    private FirebaseListAdapter<User> adapter;
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listfriends);
        list = findViewById(R.id.listFriends);
        displayUsers();

    }
    /**
     * Display all the users that have posted a chat message.
     */
    private void displayUsers() {
        Query query = FirebaseDatabase.getInstance().getReference("users").orderByChild("user");

        FirebaseListOptions<User> options = new FirebaseListOptions.Builder<User>()
                .setQuery(query, User.class)
                .setLayout(R.layout.messages)
                .build();

        adapter = new FirebaseListAdapter<User>(options) {

            @Override
            protected void populateView(View v, User model, int position) {
                // Get references to the views of messages.xml
                TextView messageUser = v.findViewById(R.id.user);
                String s = model.getUser();
                String[] array = new String[]{s};

                for (String user : array) {
                    messageUser.setText(user);
                }
            }
        };

        list.setAdapter(adapter);
        //Adds click option to every user
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListFriends.this, UserMessages.class);
                TextView message1 = view.findViewById(R.id.user);
                String tmp = message1.getText().toString();
                intent.putExtra("Username", tmp);
                startActivity(intent);
            }
        });

    }

    /**
     * On Resume of the application, this is needed due to the startlistening method.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startListening();
        }
    }

}
