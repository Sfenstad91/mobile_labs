package com.example.stian.lab4;

import java.util.Date;

/**
 * Created by Stian on 04.04.2018.
 */

/**
 * Chatmessage collection
 */
public class ChatMessage {
    private String msgText;
    private String msgUser;
    private long msgTime;

    public ChatMessage(String msgText, String msgUser){
        this.msgText = msgText;
        this.msgUser = msgUser;

        msgTime = new Date().getTime();
    }
    public ChatMessage(){
    }
    public  String getMsgText(){
        return msgText;
    }
    public void setMsgText(String msgText){
        this.msgText = msgText;
    }
    public String getMsgUser(){
        return msgUser;
    }
    public long getMsgTime(){
        return msgTime;
    }

}
