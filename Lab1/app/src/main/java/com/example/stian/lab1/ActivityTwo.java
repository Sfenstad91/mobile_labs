package com.example.stian.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Stian on 12.03.2018.
 */

public class ActivityTwo extends AppCompatActivity implements View.OnClickListener {
    //Placeholder for text from other pages
    TextView t2, t3;
    static final int A3_RESULT = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        //Initialize page elements
        Button button_next = (Button) findViewById(R.id.b2);
        button_next.setOnClickListener(this);
        Button button_exit = (Button) findViewById(R.id.back2);
        button_exit.setOnClickListener(this);
        t2 = (TextView) findViewById(R.id.t2);
        t3 = (TextView) findViewById(R.id.t3);
        t2.setText(getIntent().getStringExtra("a1Text"));
    }
    @Override
    protected void onActivityResult(int req, int result, Intent data){
        if(req == A3_RESULT){
            if(result == RESULT_OK){
                t3.setText(data.getStringExtra("a3Text"));
            }
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b2:
                next_page();
                break;
            case R.id.back2:
                startActivity(new Intent(ActivityTwo.this, MainActivity.class));
                break;
            default:
                break;

        }
    }
    //page A3
    public void next_page(){
        Intent intent = new Intent(ActivityTwo.this, ActivityThree.class);
        startActivityForResult(intent, A3_RESULT);
    }
}
