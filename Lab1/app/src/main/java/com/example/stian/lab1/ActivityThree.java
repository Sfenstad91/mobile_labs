package com.example.stian.lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Stian on 12.03.2018.
 */

public class ActivityThree extends AppCompatActivity implements View.OnClickListener {
    EditText t4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);
        //Initialize page elements
        Button button_next = (Button) findViewById(R.id.b3);
        button_next.setOnClickListener(this);
        Button button_exit = (Button) findViewById(R.id.back3);
        button_exit.setOnClickListener(this);
        t4 = (EditText) findViewById(R.id.t4);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b3:
                save_page();
                break;
            case R.id.back3:
                back();
                break;
            default:
                break;
        }
    }
    //Saves input from A3 and passes text to A2 textviews
    public void save_page(){
        Intent intent = new Intent();
        intent.putExtra("a3Text", "From A3 " + t4.getText().toString());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
    //Returns user to page A2
    public void back(){
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}
