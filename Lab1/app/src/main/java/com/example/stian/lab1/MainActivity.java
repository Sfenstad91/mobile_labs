package com.example.stian.lab1;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF = "state";
    EditText t1;
    Spinner l1;
    SharedPreferences state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Initialize page elements
        state = getApplicationContext().getSharedPreferences(PREF, MODE_PRIVATE);
        Button button_next = (Button) findViewById(R.id.b1);
        button_next.setOnClickListener(this);
        Button button_exit = (Button) findViewById(R.id.back1);
        button_exit.setOnClickListener(this);
        t1 = (EditText) findViewById(R.id.t1);
        l1 = (Spinner) findViewById(R.id.l1);
        String[] arrayL1 = new String[] {
                "Spades",
                "Cloves",
                "Hearts",
                "Diamonds"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayL1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        l1.setAdapter(adapter);
        //Sets previously selected drop down menu item
        if(state.contains("dd_choice")){
            l1.setSelection(state.getInt("dd_choice", 0), true);
        }


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b1:
                next_page();
                break;
            case R.id.back1:
                exit_app();
                break;
            default:
                break;
        }
    }
    //Next page A1 - A2
    public void next_page(){
        save_dd();
        Intent intent = new Intent(MainActivity.this,ActivityTwo.class);
        intent.putExtra("a1Text", "Hello " + t1.getText().toString());
        startActivity(intent);
    }
    //Exit application
    public void exit_app(){
        save_dd();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    //Save spinner selection
    public void save_dd(){
        SharedPreferences.Editor editor = getSharedPreferences(PREF, MODE_PRIVATE).edit();
        int dd_choice = l1.getSelectedItemPosition();
        editor.putInt("dd_choice", dd_choice);
        editor.apply();
    }
}
