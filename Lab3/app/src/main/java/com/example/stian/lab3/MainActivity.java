package com.example.stian.lab3;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import java.util.Date;

/**
 * Follow tutorial for ball logic and drawing
 * https://androidkennel.org/android-sensors-game-tutorial/
 */
public class MainActivity extends AppCompatActivity implements SensorEventListener{
    private float xPos, xAccel, xVel = 0.0f;
    private float yPos, yAccel, yVel = 0.0f;
    private float xMax, yMax;
    private static long prevVibrate;
    private Bitmap ball;

    private SensorManager sensorManager;
    private Vibrator vibrator;
    private ToneGenerator toneGen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BallView ballView = new BallView(this);
        setContentView(ballView);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        this.toneGen = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

    }

    /**
     * Registers accelerometer before screen is drawn for user
     */
    @Override
    protected void onStart() {
        super.onStart();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * Unregisterts accelerometer
     */
    @Override
    protected void onStop() {
        sensorManager.unregisterListener(this);
        super.onStop();
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            xAccel = sensorEvent.values[0];
            yAccel = -sensorEvent.values[1];
            updateBall();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * updates the balls location based on mobiles rotation
     */
    private void updateBall() {
        float frameTime = 0.250f;
        xVel -= (xAccel * frameTime);
        yVel += (yAccel * frameTime);

        float xS = (xVel / 2) * frameTime;
        float yS = (yVel / 2) * frameTime;

        yPos -= xS;
        xPos -= yS;

        if (yPos > xMax) {
            yPos = xMax ;
            wallHit();
        } else if (yPos < 0) {
            yPos = 0;
            wallHit();
        }

        if (xPos > yMax) {
            xPos = yMax;
            wallHit();
        } else if (xPos < 0) {
            xPos = 0;
            wallHit();

        }
    }

    /**
     * Vibrates(If phone has - mine did not so could not verify this),
     * and gives audio clue when a wall is hit with ball every second
     */
    public void wallHit(){
        long now = new Date().getTime();
        if((now - prevVibrate) > 1000) {
            this.vibrator.vibrate(50);
            prevVibrate = now;
            this.toneGen.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 50);
        }

    }
    /**
     * our custom ballview
     * overrides ondraw function to keep refreshing ball and screen constraints in our app
     */
    private class BallView extends View {
        public BallView(Context context) {
            super(context);
            Bitmap ballSrc = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
            final int dstWidth = 100;
            final int dstHeight = 100;
            ball = Bitmap.createScaledBitmap(ballSrc, dstWidth, dstHeight, true);
            yPos = (ball.getWidth() /2) + 300;
            xPos = (ball.getHeight()/2) + 800;

        }
        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawBitmap(ball, xPos, yPos, null);
            xMax = (canvas.getHeight() - 100);
            yMax = (canvas.getWidth() - 100);

            drawRect(canvas);

            invalidate();
        }

        /**
         * Draws our rectangle for balls borders
         * @param canvas
         */
        public void drawRect(Canvas canvas){
            Rect ourRect = new Rect();
            ourRect.set(20,20, (canvas.getWidth() -20),(canvas.getHeight() - 20));
            Paint black = new Paint();
            black.setStrokeWidth(10);
            black.setStyle(Paint.Style.STROKE);
            black.setColor(Color.BLACK);
            canvas.drawRect(ourRect, black);
        }
    }

}
